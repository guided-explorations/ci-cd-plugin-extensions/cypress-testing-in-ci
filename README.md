
End to End Testing w/ Cypress.io and GitLab
======

Intro
===
This project highlights the various ways to incorporate an automated testing framework such as Cypress.io into the GitLab CI workflow. The web application that test cases will be run against is a To-Do list web app built in React. 

App Preview
===
![To-Do App](assets/todo_app.png)

Project Directory Tips
===
`app_spec.js` file in the `/cypress/integration/` directory contains all the tests that are executed for each run

`.gitpod.Dockerfile` file contains instructions to build the docker container with cypress dependencies that are pre-loaded into the Gitpod environment during startup

`cypress.json` file contains the base url where the todoMVC app defaults to on startup


How It Works
===
There are distinct processes that need to run in order to get started. First the todo list app is a react app which runs on a nodejs web server. Once the todo list app is running the cypress app needs to be initialized and listening to the same port the todo list app receives http requests. Cypress executes tests that are designed to validate functional and UI related features of the app and reports the results. The tests are executed via GitLab CI and the results can then be integrated into the MR view.

Using Cypress in CI
===
Installing Cypress
> npm install cypress

Run Cypress in CI(Headless mode, no browser)
> cypress run

About Cypress.io
===
Learn more about [Cypress.io](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell) tool.

This project extends the [existing repo](https://gitlab.com/bahmutov/todomvc) by the Cypress team


